<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies', function (Blueprint $table) {
            $table->id();
            $table->string('phone');
            $table->enum('type', ['Vital', 'EPS Mascotas', 'SOAT'])->default('Vital');
            $table->enum('status', ['Solicitada', 'Atendida', 'Activa', 'Terminada'])->default('Solicitada');
            $table->foreignId('user_id')->index();
            $table->foreignId('promoter_id')->nullable()->references('id')->on('users')->onDelete('SET NULL');
            $table->foreignId('partner_id')->nullable()->references('id')->on('users')->onDelete('SET NULL');
            $table->foreignId('agent_id')->nullable()->references('id')->on('users')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policies');
    }
}

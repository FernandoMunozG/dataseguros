<x-jet-action-section>
    <x-slot name="title">
        {{ __('Referral Code') }}
    </x-slot>
    
    <x-slot name="description">
        {{ __('This is your Referral Code to invite new users') }}
    </x-slot>
    
    <x-slot name="content">
        <!-- Referral Code -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="idreferral_code" value="{{ __('Referral Code') }}" />
            <x-jet-input id="idreferral_code" type="text" class="mt-1 block w-full" value="{{auth()->user()->referral_code}}" disabled/>
            <x-jet-input-error for="referral_code" class="mt-2" />
        </div>
    </x-slot>
</x-jet-action-section>
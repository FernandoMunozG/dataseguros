@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
  <x-guest-layout>
      <x-jet-authentication-card>
          <x-slot name="logo">
              <x-jet-authentication-card-logo />
          </x-slot>

          <x-jet-validation-errors class="mb-4" />

          <form method="POST" action="{{ route('policies.register') }}">
              @csrf

              <div>
                  <x-jet-label for="phone" value="{{ __('Phone') }}" />
                  <x-jet-input id="phone" class="block mt-1 w-full" type="text" name="phone" :value="old('phone')" required autofocus autocomplete="phone" />
              </div>

              <div class="mt-4">
                  <x-jet-label for="type" value="{{ __('Type') }}" />
                  <select id="type"  class="block mt-1 w-full" name="type">
                      <option value="Vital">Vital</option>
                      <option value="EPS Mascotas">EPS Mascotas</option>
                      <option value="SOAT">SOAT</option>
                  </select>
              </div>
              
              <div class="flex items-center justify-end mt-4">
                  <x-jet-button class="ml-4">
                      {{ __('Request') }}
                  </x-jet-button>
              </div>
          </form>
      </x-jet-authentication-card>
  </x-guest-layout>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('extra_scripts')
  <script src="{{ asset('js/intlTelInput.js') }}"></script>
  <script>
    var input = document.querySelector("#phone_number");
    window.intlTelInput(input, {
      hiddenInput: "full_phone",
      preferredCountries: ["us", "gb", "co", "de"],
      utilsScript: "{{ asset('js/utils.js') }}"
    });
  </script>
@stop
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Policy extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone',
        'type',
        'status',
        'user_id',
        'promoter_id',
        'partner_id',
        'agent_id',
    ];

    public function promoter(){
        return $this->belongsTo(User::class, 'promoter_id');
    }

    public function partner(){
        return $this->belongsTo(User::class, 'partner_id');
    }

    public function agent(){
        return $this->belongsTo(User::class, 'agent_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ReferralCode extends Component
{
    public function render()
    {
        return view('livewire.referral-code');
    }
}

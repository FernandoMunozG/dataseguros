<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Policy;
use App\Models\User;

class HomeController extends Controller
{
    public function index()
    {
        return view('client.index');
    }

    public function policyIndex()
    {
        foreach(auth()->user()->roles as $role){
            $user_role = $role;
        }

        switch ($user_role) {
            case 'Admin':
                $policies = Policy::get();
                break;
            case 'Agent':
                $policies = Policy::whereAgent_id(auth()->user()->id)->get();
                break;
            case 'Partner':
                $policies = Policy::wherePartner_id(auth()->user()->id)->get();
                break;
            case 'Promoter':
                $policies = Policy::wherePromoter_id(auth()->user()->id)->get();
                break;
            case 'Client':
                $policies = Policy::whereUser_id(auth()->user()->id)->get();
                break;

            default:
                $policies = Policy::get();
                break;
        }

        return view('client.policies.index', compact('policies'));
    }

    public function policyRequest()
    {
        return view('client.policies.request');
    }

    public function policyRegister(Request $request)
    {
        $promoter_id = null;
        $partner_id = null;
        $agent_id = null;
        $promoter = null;
        $partner = null;
        $agent = null;

        if (auth()->user()->parent_id != NULL) {
            $promoter = User::find(auth()->user()->parent_id);
            foreach ($promoter->roles as $role){
                $user_role = $role->title;
            }
        }
        if ($promoter && $user_role == 'Promoter') {
            $promoter_id = $promoter->id;
            if ($promoter->parent_id) {
                $partner = User::find($promoter->parent_id);
                foreach ($partner->roles as $role) {
                    $user_role = $role->title;
                }
            }
        }
        if (!$partner && auth()->user()->parent_id != NULL) {
            $partner = User::find(auth()->user()->parent_id);
            foreach ($promoter->roles as $role){
                $user_role = $role->title;
            }
        }
        if ($partner && $user_role == 'Partner') {
            $partner_id = $partner->id;
            if ($partner->parent_id) {
                $agent = User::find($partner->id);
                foreach ($agent->roles as $role) {
                    $user_role = $role->title;
                }
            }
        }
        if (!$agent && auth()->user()->parent_id != NULL) {
            $agent = User::find(auth()->user()->parent_id);
            foreach ($agent->roles as $role){
                $user_role = $role->title;
            }
        }
        if ($agent && $user_role == 'Agent') {
            $agent_id = $agent->id;
        }

        $user = Policy::create([
            'phone' => $request['phone'],
            'type' => $request['type'],
            'user_id' => auth()->user()->id,
            'promoter_id' => $promoter_id,
            'partner_id' => $partner_id,
            'agent_id' => $agent_id,
        ]);

        return redirect()->route('policies.index');
    }
}

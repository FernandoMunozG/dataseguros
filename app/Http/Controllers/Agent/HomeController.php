<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Policy;

class HomeController extends Controller
{
    public function index()
    {
        return view('agent.index');
    }

    public function partners()
    {
        //abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::with('roles')->get();

        return view('agent.partners', compact('users'));
    }

    public function policyIndex()
    {
        foreach(auth()->user()->roles as $role){
            $user_role = $role;
        }

        switch ($user_role) {
            case 'Admin':
                $policies = Policy::get();
                break;
            case 'Agent':
                $policies = Policy::whereAgent_id(auth()->user()->id)->get();
                break;
            case 'Partner':
                $policies = Policy::wherePartner_id(auth()->user()->id)->get();
                break;
            case 'Promoter':
                $policies = Policy::wherePromoter_id(auth()->user()->id)->get();
                break;
            case 'Client':
                $policies = Policy::whereUser_id(auth()->user()->id)->get();
                break;

            default:
                $policies = Policy::get();
                break;
        }

        return view('client.policies.index', compact('policies'));
    }

    public function policyRequest()
    {
        return view('client.policies.request');
    }

    public function policyRegister(Request $request)
    {
        $promoter_id = NULL;
        $partner_id = NULL;
        $agent_id = NULL;

        if (auth()->user()->parent_id != NULL) {
            $promoter = User::find(auth()->user()->parent_id);
            foreach ($promoter->roles as $role){
                $user_role = $role->title;
            }
        }
        if ($promoter && $user_role == 'Promoter') {
            $promoter_id = $promoter->id;
            $partner = User::find($promoter->parent_id);
            foreach ($partner->roles as $role){
                $user_role = $role->title;
            }
        }
        if (!$partner && auth()->user()->parent_id != NULL) {
            $partner = User::find(auth()->user()->parent_id);
            foreach ($promoter->roles as $role){
                $user_role = $role->title;
            }
        }
        if ($partner && $user_role == 'Partner') {
            $partner_id = $partner->id;
            $agent = User::find($partner->id);
            foreach ($agent->roles as $role){
                $user_role = $role->title;
            }
        }
        if (!$agent && auth()->user()->parent_id != NULL) {
            $agent = User::find(auth()->user()->parent_id);
            foreach ($agent->roles as $role){
                $user_role = $role->title;
            }
        }
        if ($agent && $user_role == 'Agent') {
            $agent_id = $agent->id;
        }

        $user = Policy::create([
            'phone' => $request['phone'],
            'type' => $request['type'],
            'user_id' => auth()->user()->id,
            'promoter_id' => $promoter_id,
            'partner_id' => $partner_id,
            'agent_id' => $agent_id,
        ]);

        return redirect()->route('policies.index');
    }

    public function whatsapp($id){
        $policy = Policy::find($id);
        $url = "https://api.whatsapp.com/send?phone=" . $policy->phone . "&text=Hola, Soy " . auth()->user()->name . " de DataSeguros. Te contacto por tu solicitud de una póliza " . $policy->type . " con nosotros. ¿Cómo puedo ayudarte?";
        $policy->status = "Atendida";
        $policy->save();
        return redirect($url);
    }
}

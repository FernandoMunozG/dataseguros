<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Agent\HomeController;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for administrators. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', [HomeController::class, 'index']);
Route::get('/partners', [HomeController::class, 'partners']);
Route::get('/policies', [HomeController::class, 'policyIndex'])->name('policies.index');
Route::get('/policies/request', [HomeController::class, 'policyRequest'])->name('policies.request');
Route::post('/policies/register', [HomeController::class, 'policyRegister'])->name('policies.register');
Route::get('/policies/whatsapp/{id}', [HomeController::class, 'whatsapp'])->name('policies.whatsapp');
